import Vue from 'vue';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import VueResource from 'vue-resource';
import VueCookies from 'vue-cookies';
import 'element-ui/lib/theme-chalk/index.css';
import Home from '@/views/Home.vue';
import Login from '@/views/user/Login.vue';
import User from '@/views/user/User.vue';
import Order from '@/views/user/Order.vue';
import Room from '@/views/room/Room.vue';
Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(VueResource);
Vue.use(VueCookies);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/room/:id',
    name: 'room',
    component: Room
  },
  {
    path: '/user',
    name: 'User',
    component: User
  },
  {
    path: '/user/order',
    name: 'Order',
    component: Order
  }
]

const router = new VueRouter({
  routes
})
export default router
